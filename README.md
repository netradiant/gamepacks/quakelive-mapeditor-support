# Quake Live map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Quake Live.

This gamepack is based on the game pack provided by NetRadiant-custom.

More stuff may be imported from http://svn.icculus.org/gtkradiant-gamepacks/QLPack/ in the future.
